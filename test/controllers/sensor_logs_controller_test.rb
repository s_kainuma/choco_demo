require 'test_helper'

class SensorLogsControllerTest < ActionController::TestCase
  setup do
    @sensor_log = sensor_logs(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:sensor_logs)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create sensor_log" do
    assert_difference('SensorLog.count') do
      post :create, sensor_log: { hermidity: @sensor_log.hermidity, infrared: @sensor_log.infrared, log_datetime: @sensor_log.log_datetime, sensor_id: @sensor_log.sensor_id, sequence_num: @sensor_log.sequence_num, sunshine_degree: @sensor_log.sunshine_degree, temperture: @sensor_log.temperture }
    end

    assert_redirected_to sensor_log_path(assigns(:sensor_log))
  end

  test "should show sensor_log" do
    get :show, id: @sensor_log
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @sensor_log
    assert_response :success
  end

  test "should update sensor_log" do
    patch :update, id: @sensor_log, sensor_log: { hermidity: @sensor_log.hermidity, infrared: @sensor_log.infrared, log_datetime: @sensor_log.log_datetime, sensor_id: @sensor_log.sensor_id, sequence_num: @sensor_log.sequence_num, sunshine_degree: @sensor_log.sunshine_degree, temperture: @sensor_log.temperture }
    assert_redirected_to sensor_log_path(assigns(:sensor_log))
  end

  test "should destroy sensor_log" do
    assert_difference('SensorLog.count', -1) do
      delete :destroy, id: @sensor_log
    end

    assert_redirected_to sensor_logs_path
  end
end
