json.array!(@sensor_logs) do |sensor_log|
  json.extract! sensor_log, :id, :log_datetime, :sensor_id, :sequence_num, :temperture, :hermidity, :sunshine_degree, :infrared
  json.url sensor_log_url(sensor_log, format: :json)
end
