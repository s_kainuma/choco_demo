require 'rack/contrib'
require 'date'

class API < Grape::API
  use Rack::JSONP
  format :json

# APIアクセスに接頭辞を付加   
# ex) http://localhost:3000/api   prefix "api"   
# APIアクセスにバージョン情報を付加   
# ex) http://localhost:3000/api/v1   version 'v1', :using =&gt; :path
  resource "users" do
  # ex) http://localhost:3000/api/v1/users
    desc "returns all users"
    get do
      User.all
    end
    # ex) OK: http://localhost:3000/api/v1/users/1
    # ex) NG: http://localhost:3000/api/v1/users/a
    desc "return a user"
    params do
      requires :id, type: Integer
      optional :name, type: String
    end
    get ':id' do
      User.find(params[:id])
    end
  end
  resource "mobile_locations" do
    get 'by_hour' do
    query = params[:date] + ' ' + params[:hour] + '%'
#    select = "sensor_id, temperture, hermidity, DATE_FORMAT(log_datetime, '%Y-%m-%d %H:%i')"
    select = "sensor_id, temperture, hermidity, log_datetime"
    group_by = "DATE_FORMAT(log_datetime, '%Y-%m-%d %H:%i'), sensor_id"
    sensor_logs = SensorLog.where('log_datetime LIKE ?' ,query)
    sensor_logs = sensor_logs.select(select)
    sensor_logs = sensor_logs.group(group_by)
    temp_avg = sensor_logs.average('temperture')
    herm_avg = sensor_logs.average('hermidity')

    mobile_locations = []

    tmp_heatmaps = []
      sensor_logs.each do |sensor_log|
        #tmp_dataを組み立てる
        tmp_data = {}
        tmp_data[:sensor_id] = sensor_log.sensor_id
        val = sensor_log.temperture.to_f;
        tmp_data[:temperture] = (-39.4 + 0.01 * val)
        val = sensor_log.hermidity.to_f;
        tmp_data[:hermidity] = -2.0468 + 0.0367*val - 1.5955*val*val/1000000
        str = sensor_log.log_datetime.strftime("%Y-%m-%d %H:%M:%S")
        tmp_data[:datetime] = str
        #tmp_heatmapsに溜め込む
        tmp_heatmaps.push(tmp_data)
      end
      tmp_heatmaps_result = {}
      tmp_heatmaps_result[:datas] = tmp_heatmaps
      mobile_locations << tmp_heatmaps_result
    end
  end
end
