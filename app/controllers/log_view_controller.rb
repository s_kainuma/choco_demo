class LogViewController < ApplicationController
  before_action :set_sensor_log, only: [:show]

  # GET /sensor_logs
  # GET /sensor_logs.json
  def index
#    @sensor_logs = SensorLog.all
#     @sensor_logs = SensorLog.where('log_datetime LIKE ?', '2015-07-25%')
     if params[:datepicker] != nil
       @sensor_logs = SensorLog.where('log_datetime LIKE ?' ,params[:datepicker] + " " + params[:time] + '%')
     else
       @sensor_logs = SensorLog.all
     end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_sensor_log
      @sensor_log = SensorLog.find(params[:id])
#      @sensor_log = SensorLog.find([2,5,10])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def sensor_log_params
      params.require(:sensor_log).permit(:log_datetime, :sensor_id, :sequence_num, :temperture, :hermidity, :sunshine_degree, :infrared)
    end
end
