class SensorLogsController < ApplicationController
  before_action :set_sensor_log, only: [:show, :edit, :update, :destroy]

  # GET /sensor_logs
  # GET /sensor_logs.json
  def index
    @sensor_logs = SensorLog.all
  end

  # GET /sensor_logs/1
  # GET /sensor_logs/1.json
  def show
  end

  # GET /sensor_logs/new
  def new
    @sensor_log = SensorLog.new
  end

  # GET /sensor_logs/1/edit
  def edit
  end

  # POST /sensor_logs
  # POST /sensor_logs.json
  def create
    @sensor_log = SensorLog.new(sensor_log_params)

    respond_to do |format|
      if @sensor_log.save
        format.html { redirect_to @sensor_log, notice: 'Sensor log was successfully created.' }
        format.json { render :show, status: :created, location: @sensor_log }
      else
        format.html { render :new }
        format.json { render json: @sensor_log.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /sensor_logs/1
  # PATCH/PUT /sensor_logs/1.json
  def update
    respond_to do |format|
      if @sensor_log.update(sensor_log_params)
        format.html { redirect_to @sensor_log, notice: 'Sensor log was successfully updated.' }
        format.json { render :show, status: :ok, location: @sensor_log }
      else
        format.html { render :edit }
        format.json { render json: @sensor_log.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /sensor_logs/1
  # DELETE /sensor_logs/1.json
  def destroy
    @sensor_log.destroy
    respond_to do |format|
      format.html { redirect_to sensor_logs_url, notice: 'Sensor log was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_sensor_log
      @sensor_log = SensorLog.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def sensor_log_params
      params.require(:sensor_log).permit(:log_datetime, :sensor_id, :sequence_num, :temperture, :hermidity, :sunshine_degree, :infrared)
    end
end
