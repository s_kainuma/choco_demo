# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150805055229) do

  create_table "log_views", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "sensor_logs", force: :cascade do |t|
    t.datetime "log_datetime"
    t.integer  "sensor_id",       limit: 4
    t.integer  "sequence_num",    limit: 4
    t.decimal  "temperture",                precision: 10
    t.decimal  "hermidity",                 precision: 10
    t.decimal  "sunshine_degree",           precision: 10
    t.decimal  "infrared",                  precision: 10
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
  end

end
