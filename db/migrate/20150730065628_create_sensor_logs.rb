class CreateSensorLogs < ActiveRecord::Migration
  def change
    create_table :sensor_logs do |t|
      t.datetime :log_datetime
      t.integer :sensor_id
      t.integer :sequence_num
      t.decimal :temperture
      t.decimal :hermidity
      t.decimal :sunshine_degree
      t.decimal :infrared

      t.timestamps null: false
    end
  end
end
