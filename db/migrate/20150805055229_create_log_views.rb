class CreateLogViews < ActiveRecord::Migration
  def change
    create_table :log_views do |t|

      t.timestamps null: false
    end
  end
end
